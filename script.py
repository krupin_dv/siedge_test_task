#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

from tracking_path import TrackingPath, TrackingPathException


parser = argparse.ArgumentParser(description='Скрипт отслеживания папки')
parser.add_argument('path', metavar='path', help='tracking path')

args = parser.parse_args()
path = args.path


try:
    tp = TrackingPath(path)
except TrackingPathException as e:
    print(e.message)
    exit(1)

prev_files = tp.get_prev_files() or set()
current_fiels = tp.get_files()
tp.save()

new_files = current_fiels - prev_files
if new_files:
    print('New files: ')
    print(', '.join(new_files))
    print()

deleted_files = prev_files - current_fiels
if deleted_files:
    print('Deleted files: ')
    print(', '.join(deleted_files))
