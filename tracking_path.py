# -*- coding: utf-8 -*-
import os
import hashlib
import json



class TrackingPath:

    path = None
    hash = None
    filename = None
    filepath = None
    location = None

    def __init__(self, path, location=None):
        if 'unicode' in __builtins__:  # for Python2
            path = unicode(path,  encoding='utf-8')

        if not os.path.exists(path):
            raise TrackingPathException('Error: path "%s" not exists' % path)

        if not os.path.isdir(path):
            raise TrackingPathException('Error: "%s" is not directory' % path)

        self.path = path
        self.location = location or os.path.dirname(__file__)
        self.hash = hashlib.sha1(path.encode('utf-8')).hexdigest()
        self.filename = '%s.json' % self.hash
        self.filepath = os.path.join(self.location, self.filename)

    def get_files(self):
        files = [f for f in os.listdir(self.path) if os.path.isfile(os.path.join(self.path, f))]
        return set(files)

    def get_prev_files(self):
        if not os.path.exists(self.filepath):
            return

        with open(self.filepath) as f:
            files = json.loads(f.read())
            return set(files)

    def save(self):
        files = list(self.get_files())
        with open(self.filepath, 'w') as f:
            f.write(json.dumps(files))


class TrackingPathException(Exception):
    pass